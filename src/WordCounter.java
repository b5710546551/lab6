import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
/**
 * WordCounter can count how many times that this word occurs.
 * @author Chinatip Vichian
 *
 */
public class WordCounter {
	/** Map  collects words and times that it occurs */
	private Map<String,Integer> collectWords = new HashMap<String,Integer> ();
	/** 
	 * Add word to the collectWords of Map.
	 * If it already occurred, it will not save a new one to the Map.
	 * But, it will increase the words count.
	 * @param word is string that come add to the WordCounter
	 */
	public void addWord(String word){
		String wordLowerCase = word.toLowerCase();
		if (collectWords.containsKey(wordLowerCase)){
			int count = collectWords.get(wordLowerCase);
			collectWords.replace(wordLowerCase, ++count);
		}
		collectWords.put(wordLowerCase, 1);
	}
	/**
	 * Get set of words
	 * @return set of words that come to WordCounter which collected by addWord method
	 */
	public Set<String> getWords(){
		return collectWords.keySet();

	}
	/**
	 * Get number of occurring
	 * @param word which come to check how many times it occurs
	 * @return
	 */
	public int getCount(String word){
		return collectWords.get(word);
	}
	/**
	 * Get Array of key that already sorted
	 * @return sorted key array by alphabet
	 */
	public String[] getSortedWords(){
		String[] sortedWords = new String[collectWords.keySet().size()];
		sortedWords = collectWords.keySet().toArray(sortedWords);
		Arrays.sort(sortedWords);
		return sortedWords;
	}
	
}
